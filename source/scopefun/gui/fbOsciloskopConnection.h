#ifndef __fbOsciloskopConnection__
#define __fbOsciloskopConnection__

/**
@file
Subclass of Connection, which is generated by wxFormBuilder.
*/

#include "osc.h"

//// end generated include

/** Implementing Connection */
class OsciloskopConnection : public Connection
{
protected:
    // Handlers for Connection events.
    void m_choiceConnectionOnChoice(wxCommandEvent& event);
    void m_textCtrlIPOnTextEnter(wxCommandEvent& event);
    void m_textCtrlPortOnTextEnter(wxCommandEvent& event);
    void m_checkBoxConnectedOnCheckBox(wxCommandEvent& event);
    void m_buttonConnectOnButtonClick(wxCommandEvent& event);
    void m_buttonDisconnectOnButtonClick(wxCommandEvent& event);
    void m_buttonOkOnButtonClick(wxCommandEvent& event);
    void m_buttonDefaultOnButtonClick(wxCommandEvent& event);
    void m_buttonCancelOnButtonClick(wxCommandEvent& event);
public:
    /** Constructor */
    OsciloskopConnection(wxWindow* parent);
    //// end generated class members

};

#endif // __fbOsciloskopConnection__
